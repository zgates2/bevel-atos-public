**Reminders**:

- It's your responsibility to investigate the customer request and communicate your understanding of the request. The business case should represent the customer request to demonstrate understanding of the Demand.
- The workflow for a Demand is the following: Complete a JAD session/Requirements gathering meeting with the agency and gather all the requirements needed before you write the Initial Business Case/Demand is in Screening State. That means you'll need to meet with the person requesting the demand to clarify the project goals and understand what problem they're trying to solve. If the problem is unclear, the solution may be off target, so it's critical that you understand the problem before you move on to defining requirements.

After writing the Initial Business Case, the agency should provide the initial/Screening approval. Conduct a Peer Review, make changes or updates from the feedback that was provided. If your Demand is approved during Peer Review, you will schedule the Demand for Engineer Review Board (ERB).
You must get approval from ERB, if ERB rejects your demand, you should follow the instructions given to make updates/changes for approval. Then, schedule for another ERB review during the meeting times.

Strive for Peer Review and ERB approval on each Demand.
ERB’s rule of thumb is to have your ERB entry 48 hours before the Review date you need.

- If you size/re-size the Demand by using the ‘Sizing Matrix’ document. Attach the document to the Demand.
- Include a status of the Demand every week, even for a customer email that is included, have a clear status of the Demand. (Anyone should be able to read the note and understand all the details of your demand status).

E.g.: Demand - TPWD_DMND5351_RFS_Move TREC Domain to Azure Cloud

Business Purpose – Establish a cloud presence and modernize the TREC domain.

Solution Overview – TPWD would like to move the County Tax Assessor Collector's domain from the ADC to the Azure cloud. The new environment will contain (2) 2016 Windows Domain Controllers and (2) 2016 Windows Application Servers supporting web based RDS.

Schedule - Solution is Pending Agency review and approval.

Current Status – The demand is currently in Pending Agency review and approval as of mm/dd/yy.

- Don’t forget to attach the PDF version of your Visio Diagram.
- Agency is responsible and accountable for supplying firewall rules to support this deployment. FW Rules should be provided by customer during the initial stages of solutioning. If customer does not provide the FW Rules, in the dependencies/constraint indicate the customer decided not to give FW Rules during solutioning.
- Please gather the accounts that the customer initially wants populated during build for ULN server solutions. This will document the approval of the request and eliminate the back and forth and potential PCRs.

- This document does **not** include instructions for resource plans (Refer to time tracking PowerPoint in STS) or cost plans.
- DIR requested that when we make Solution changes (PCR – Project Change Request) that will alter the cost, especially after it has been reviewed with the Agency, that we add a note of what changed in the Notes tab.
- Attach all quotes to DMND tickets as part of the solution
- If you have a Demand for an existing asset/configuration item. Ensure you associate that Demand by including the existing CI name in the Configuration Tab.
- Include the Implementation Start Date and End Date as provided by the TPM before sending the Demand for the customer for final approval.

**Business Case**

**Request name**: enter the ‘Name’ of the Demand - Agency_DMND####\_RFS/Refresh Demand Name

**Solution Description**: This description explains the major characteristics of the product or service of the Demand and describes the relationship between the business need and the product or service requested.

- Add in the Business justification, this should help anyone understand from business perspective why the agency submitted the Demand.

- E.g.: Agency has submitted this DMND 0000### to support **\*\***\_**\*\***. They have requested Atos to build the following:

One (1) Linux Production Fully Managed Red Hat 7.x instance deployed on the dedicated (VCE Blade UCSB-B200-M4-U APINVHVMxxx & APINVHVMxxx) ESX.The resource requirements are: xx vCPU xx GB memory, Disk Space will be xxx GB total storage allocation on Tier 2

One (1) Semi-Managed Fractional AIX OS 7.X on the ADC Fractional Intel Infrastructure

The resource requirements are: 2 vCPU (0.5 PU Capped) 8 GB memory, Disk Space will be 100 GB total storage allocation on Tier 2

One (1) Managed database server Windows 2012 R2 on the SDC Fractional Intel Infrastructure will be installed along with MSSQL 13.X (Server 2016) Standard Edition.The resource requirements are: 4 vCPU, 32 GB of RAM, 640 GB of total storage allocation on Tier 2

One (1) Managed DBaaS instance, Highly Available Oracle 12.X (12cR1), deployed on the Solaris SuperCluster platform

The resource requirements are: (Two Cores -1 each on two nodes), 16 threads (8 on each of two nodes) which equates to 16 vCPU. In addition, each of the two nodes will have (standard 6GB + additional 58 GB = 64GB) which equates to 128 GB memory, Disk Space will be standard 200 GB plus three (3) additional Shared Instance Storage of 200 GB each for a total of 800 GB agency total storage allocation

This solution will utilize common SAN storage provided in the consolidated center. The data communications networks are operational and have resources sufficient to provide the requested service.

**In scope**: Is this Demand in scope of the DCS program? Yes/No.

**Out of scope**: Is this Demand out of scope of the DCS program? Yes/ No

**Assumptions**: events, actions, concepts, or ideas you believe to be true and plan for. You can make assumptions about many elements of the Demand, including resource availability, timing of other related events, availability of vendors, and so on. It's important to always document and validate your Demand assumptions. Any key assumptions should be entered this section. E.g.: “DCS customer will install configure applications.”

**Solution Design**:

- Any key instructions should be written here, the submission should be written with clear details and written so that any of the Solution Architect can move forward with your demand. Solutions blocks only apply to servers. If you are writing a proposal for rate card, statement of work, or any other Demand, you do not need Server Blocks. The customer is not always able to articulate what the problem is, and their request may be vague and loosely worded. Your job as project manager is to figure out what the customer really means
- If applicable, refer to the technology exception here.
- Include the total storage needed for your demand:

| ADC storage total | 0 GB |
| ----------------- | ---- |
| SDC storage total | 0 GB |

- You should be creating server blocks from the SA Requirements tool version x ‘Blocks’ Tab. Every Cell in the ‘SA worksheet tab’, should be considered and completed.

- CI Name should follow the program naming standard. Refer to Naming Standard- w Cloud Excel Document.

- Number of IP’s should be included. How many Temporary/ LB/Internal/ External IP (# for each), BUR IP (# of IP’s, if needed).

- Include the details of any SSL Certificates, if needed.

- Indicate if there are SSL Certificates are REQUIRED / NOT REQUIRED for this solution.

- SSL Certificates will be procured by Agency / Atos.

- SSL Type:

   ![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAhwAAACcCAYAAAA00XKNAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAADXWSURBVHhe7Z0Bttu2rrXT3lG9ptNJ+maT3Lncng7lrTb/VHrzYyMACkIgJR/bOZazs9a3IgokCFI+wjYlS+/k31dCyHn488v/+/rTTz+1NkIIeTT+78+/vsq/f8n2O/mfEEIIIeS2/OtfITwoOAghhBByHyg4CCGEEHJ39gXHl3dfP/4Slb6+k+0X2XdJnY+270tus8OXl3dff0k+sb3p90o+m/8XK3/5LP14f7Jd7W/Fy8d/5mGD2Lo2b0Gdv67OvdHPWkWO4+cbf3YIIYRcxq7g0KSLZI8yhIWdwLN42KtzqeBA4kIsH0V0+D5P/tckDoiYjykRVkFR47yl4Kh9vxaf60vE2/fi0uN8DzYx+OdRoOiYc6vPJyGEzNgVHEeSyF6dSxOR1r/1N3dJNvrte+H30jgPc6Dvo1BwrOli6AQsSeDz+dO3z+d/OzshhNyAY4JD+EVORrNEslfnokQkSQG+dpODnCR9BaLr2/v0yzL58gzAqkVewci+AL4NZ7v3mS8doc8cT72s5N+oq2/1txP/DG0j5LqbOMsc+lx8lD68v2F+d2L5nNoNY851UnuAsddjEOPO/sQWl8rkfxdmuQ5izXO7+mx0n7UqOC6Oy+yzY1/7rMdj2l/xFzG/Zh5W/oSPEBRir/5gq8fu5b//tCOEkFuxKzj8ZO1g2TWfyI7U6ZLAjKPfRt2nJgUkg9KHloVYRk8ncfcxSwzuo7XbiRxL0LD5vQrZtvHV9O32WfwzNKZSr86Z3/PhY1ffye7jGuyTWLLvvWNTx+D9xjFI+7yvYY6lrPOU7F72Pj3h+jGp1BhiPKnNxXG5fXXsU59tW2HTn9Rrj7+UN/MAwSDlzTyYOHBBEf6s/Jf3Z+0//zXWV7v7l/FxhYMQci/2bxoVcIL1E6DiJ8aDdeoJecUhwVFO+MDb1SQafeKkin0p6beJIbUZ7NbnKi7MAb5B+jfZaFv7PhD/DI0pxagU/3Ucm7nIY9mJxbfRHvWHfgtH+819DfuaY7Q8Jg1avyJtumR/OC7bnh37vRiP9Dcc/24eioCIPiA4xN9PjT/sawVGbY/+KDgIIXfmkOBwXuQkpidCYXby7epsTrgr7GQ88w8iCTZM+2xO4nuJIdu9z2nSsbq6uiN9qS9rW/s+Ev8MjSnFOOyX9viGi75y8lklvENzab4V2Z6JotpPLbdzmGK5meBI9TsujWv32Bd/e5+r3Tnv5mEhGOAP4qL6AnEMKTgIIW/MRYID7J18Qa1zJAlktH462W4Qv/Cfk2pl02dzEt9LDIPd+mzH3dgGX7XvA/HP0JhSjIHHIH3g/yPf6DXeg7FARMX1/8mxqf3M+h36yvuaY7Q8Jg2bPhsujsu2Z5/5vRgP9Zfp5mElGMRfXeGoUHAQQt6ateCwEyNORNMT/oE6R5JAphM17tMTqfvUa9bJvupT96WT+F6bpT0njTQHqJe/wQ5tU9/uaxb/DK3nMRTUp/ndjFvw+ZyNq4vFx5KX+meJzf1437Uc+wS/12CIxfx38+Q+9uap67NycVy1TT72Um7HMGtb9rXHv5uHlWBI9o2/Yl+2l/4oOAgh9+LQTaN+AlXkRFWX1Pfq+Mm8svpGVu8J2fvVQD7Zgu4knx+ghbrDSb5pU+21z/xLgOHhXFLHVwN8Hmrfe/HP0JiEPC7H+6jz6uPKv1AYjuFOLLld/fVDps5fdwzQ1+Av9yX/bxJt8bE5JoW2z8LFcZl9duwhQPyziv1ebyU4lnMu/2/mYUcwLP0daD98Pu2+D0IIuSUXX1Ihj4snjbr0fyQJE0IIIfeEguOJUGEh1G//FByEEELeGgqOZ8DuKQDdjY0UHIQQQt6ajeAghBBCCLkjFByEEEIIuTu8pELI2fjpp3df/+u/VCGEkAfHLqtQcBByNig4CCFngoKDkJNCwUEIORMUHISclFZw4MFheNDXt2ulj/sEUYnzfYrz4+9NnKWO88un7zwmi+N3ijtCroKCg5CTshEcL7JP/o7jCaSCPlX0AUXHbxJ7iAzELWV/u21giT6PB6DtdxcdhJCroeAg5KRUwYFHmG+exWIrHptk/pZAYIgQ+isLo/ffYh9ExERwuEDhigMh54KCg5CTMggOJOGyutFiAgR/70CFiO0bVkZMAGBb38Vi9SPJQwyIaPB3uOh+EwKDb/O3x7Di4cwEh4D64d/qeb+fUr86Drzvye1Y7RF71EfZ/Zf4w2b+dYy2/fnTP3V1tWXiA7FwJYaQb1BwEHJSNoIDqwalTmVYBUEb+btHQsell9hvYuIL9ifhgRc1xsqEJV6IhOw7RIDH4/EtGPxmm/XRCY68ItJdnnFhhHrZN+rqi+xQdvFgMXd+VDBYvSw4QmSU/lwIhQ/p/8gcEPIjQMFByEm5WHBIsqwrGS4SNOnbvR76BmjBE21XvyZrt2UBcgTtV849nrAHmv6dEBxNnbz6ofXSysleORPiwfoYVjiwH/WyzdrM/BHyo0PBQchJ2QgOW62o9YJGlMTKhiROvJYf79zBvs9S9mSKc0MmC47aH0SH19tLvC42smgZmPQBQlT4mFKdLCIuEhzNeGcrHCGQallAbBgX2n/YmQNCfiQoOAg5KYPgEHSFAeIh1Rn2S3KcrXBg++WjbEs9FR4pubYiZmUDEAJiz4k4s1zZcGZ9ZN9NnVetcJifLH5WKxwRdy1nduaAkB8NCg5CTkoVHN2Kgf4sFisAVh5ECRKi1I9kbWW9nGL1NSF7/Zzc83ayRd+rZFvrzqh9GBAC+WexKIeIKP0eFhyl3ctv3+biohWONK4uFkJ+dCg4CDkpVXAoSHLyt4y/Z8Xuywi7JMXNr1SSHYKkJnjs29S35DrUtQQ78+14Mvd6ThYCivVxab3Nr1SOCA7b9rggaPRyE2zmf1dwoFzmgL9SIeQfKDgIOSmt4CCEkAeFgoOQk0LBQQg5ExQchJwUCg5CyJmg4CDkpFBwEELOBAUHISeFgoMQciYoOAg5KRQchJAzQcFByEmh4CCEnAkKDkJOSic4/D0oWv4yPnOjewopeQziqaaN7Vb89vMd+vDnkPzd2J4Mnb8/73uMApnXX6U/zOszPceFgoOQk9IJjvzgru6porOHcZErqQ8Au5DTCI4qMCg4yAVQcBByUqrgGFY3IDDSI83B8Ap6clsoOLZ1nwwKjuuh4CDkpFTBEW95TXUyw4pHRtrg0stnESs4D4D8PpX6uO78uHR9NDiETrZZEopyihGJ1f0Mybn0kR9PPjCrZ33Wd7t4H4gT44u2Ka6Vzf3k/nTs2I+xy5y7bVNHwHj9cenT8X549/WDbLdioIxjKNv2p0//+NJ3zHhd6eNn29/1gQQasWUBYeOCrQqJ3EbH6fH8/k8M+obcNNbaz2aMzh8pXvjPyb3YMJ6/rY9//yrxyrjj8iFsEhsuSdS6IMfzn714kg+dP49Jxg3/MRcp1hyP2lM8UfZ4ch/ZZv51vmw7H+f/ke08prNAwUHIScHJJydGnODqe1AcfbFbWfEIrO2wOiJlT3IqVCSJZBsSF8pI1vpyuFQ3yuIXySjXheDBiTnisXb+DT/6SLbMtJ71tRIcONl7GX78BXBTm5SxHe9eyfNi/sNW+gP/xnjN/uXf45iyX7VJOQuVoPrNZdsOkZHjk7pIrLM+NrFJOc9jjKvi/WeBIuVIlBIDhIELi+hHbOjnZyn/afFVEG8kdCRi1LV+Yixoa+LDkzwSPOY2142yxKfJOtXdxONjKWif/0l1U5/Z5qLBxUsbz//08XR+1Gb1suDAcdY5Lv3VuB8ZCg5CTgoSiJ6oUEaykb/hTnD4W2RnYgQnQV3h8EQuZSSRWX2Iiioi3JZFxVBufGbxMCT2BdN61b+VPflqHKmdzomtZExtElv2ATxmH49uw1b625RzWxwr6TMLqrBZOVj5te1h1cVt0geS6dCHJKkce4iGxtaKH1DbzmKwRPnek6a1134s2Q5+zeZioNoq2U8WEbBNyxJPJPHGj/tWiuAZ6u74ORxPM862D9t2kYI5rv2fBQoOQk7KRnDgG3Sp42IDySTvH5AT2LA6ImUkjVrGOcJxf5cKjuwj+wFIur5fT8i2v9LWm8TsibqKCp0v2YeEPLXJvioM4ht78V/76+YMIDHX1R1wjeDobMOqhfnSZIY+FrHFuFKiHaj2RVl9SZ+bfroEbyBGfF5Rb7g0I76QZDs/hxN84yP78RhAt/oRYkA+H9WW+zwcD8oWk4854rH9Po+DwKjlE0HBQchJGQSHnITqJZXdlQ2ntpUykoaWbTsLg1utcExBwpe6kUhn5HrVv5XdRxUVt1rhCNteOYO4pc97Cg70cckKR7CydfZFWft5bWK0yybuBwk2C4NI/rJ9OMGbn0PxfI8VjtW4ch+1v0vG8WDsCw754MK2uhnN+Xiw3mmQscTNcEBOQm29SzG/L1Z+Eb/RR9r/UNS5SFx9zPN8lLkhc5aCw8pZKExp2iJpaLkkf3xW84rJYcFRbbkP245YS5/BTj0kbT2Ry7aKhmRD33lVIddd2fL20J/FEjGm2LSukO+TqPWHWLEaIeXZZYyu7iqG6EOS0qyPNjYk17wNW8Xs3SWUbPdkmO/hUNssUZotkm8WHHlbbH/8ZmO5NMFLebBJn6vErfP3yns4DsUjn6fcDuOKPnJsNc6duB+ZXcGhP6VDQjyQbO8iOMTfmyQg+TBgTnK/mItXiY6dMdx03u41XzO/Nk9XxX+vmJ8cnHT9JAbyz16riHWyOAgw/+KrFRxSRkL29rixNPfjIsJ9DaKiKSN5uq9BDEkdjMdtSGhhyyzqucjAftxgh5WYPAY9j1k7v2F0aitz4bZNok3zj+QKMabJxPZhvL5cHm1B8uuxZrGScbGQ62ahNhMce30gaW5icx+SzLz/io9TL3nU+qnc9uOCosOEBerVukjS7gNj+YRjZkn8cIK38uF4ZCxI7KiLX4Xo/Hn9ZOtiPRoPtv3Yoo8Yl/nXeczbqW8vQ6i8l8/+GX61shYcMjBNBP5/tReeSXBgLJuTs8Vy8RhXY7j1+O41Xwu/OQG9invF/OTgROUnMYCEG780IQMqKnAiv9BGCLkdS8GBE5h/o8c3ps3JTJIM2iliV8Eh+2ryiISU61sbrWMJpz4HADb49H2R6Gd+ki/fX5Nh9jdNcOb/SAJs/SEG9Cu4zdExWIwu5NzmzzMY/GR7GsdsDtr5KvuHcRU/UzHlMTe2YY5LzIO/mS37bra7z8XGX3Osn50qOED+Vk/+gYKDkLdnKTiQpCIpIDHJCf1LsfsJXsWJ1d8kIGuX63uiU/+WOCKZmG2TgIypn2LzmLyc41JbGU8AnzNbYurPYo4Y6xhyebYtDONc2RCvlPNcRl/CatzwszrGQeNXsb5Xce3asu9mu/1cCNlfPdY/Ap3gwDxsvhgQCg5CHoC54MDJvSSfTcJo7EheevK3b9yrE6DX98QSic/KmwQ0IfysYm78RLu0T2n8bFj5M1s7nlqebR+JIVH7bvuqdW37UJI2P/icVKbjFKZxzWzNdut/dazTvmemExyEEPKoTAUHvhV7QhlIQmImODQxmA1+asLI/tpklMvVlvZVP11M8e2+aePtwq+DZCa2oc/Kyp/Z2vHU8mS7G8uA1T3ad64Xda0Ojpvvnybs6rejEQEx/ytb9j3bLuXlsU77nhkKDkLImegFxyy52H5NVk0CCcEh27jnQ+/n8Dq5ba1vti6xzGytn0lMm6R2gGg327/yV22r8my7GUtg9S6eyz3Q56zuET9NnWlcM9tsu5ab+Zkds2eFgoMQciZawaE/qbOVjEq+eTSf4PUbp5QjCVryml1/95/t7SZJ265+Wz9S7mLy8vANuPZZ2IxHQPuc5Kb+qm8rh69Z3dJuSKDZh+ybzkGuZ/a9OKNu8TuQ2y0YYi7+prYmps12U14d6x8BCg5CyJloBQdO5DlhDTSJAmUIi9oO5UgWgibsVD8S4U5i8XaeTKZ+Ulu1SyLuYvK20zE6NtZA/NU6rb8SPxjGkO2z7VR2/zmZruZg6MvqT8ddxjidkxrbjBLz4G9my75n24uy+muO9bPzJoJD5hfPWfguv4TZ62tlN1t+TgY5H3hmRjz7gpye+U2jT8KPloR+ZCg4vgN7IuA1THz6Q8KmomEVi9neTHBc0v81sb71OO8MBcdz8VyCQ/748I03ko59e9/9Vk7OB4/10woOXB7D2H75aCtas75WsZiNguPcUHA8F0+3wuHX8h2ubjwvP/qx7gQHHvzl87EUBTJXeJy51pXEPlyWXGEJ7uX3b/2jPZ5hUe0ew+rx5W6rjzv/IvX8RnPtazaOSSyanMz2e2cDJZbhUeplDMtndDR+ULd9pHmp++6D7JexzR5/nvfPBEVt62It/NhYhse1Oz5HzWPJtSzxDo8aT7HtUh5TjrHqo7fRh4iITxKPz8UQW24nbT7INgXH8/D0l1QIeVZwws5JJIsGTTxSrq+rdyBMhrry93+J4IBIyUnUE6L69QRdbEiOIUBgQ3yehMS2ERaz/cWusaBO7m9lk7abWN73L29zP6uEDz8xXvfTtBv8WjyaxJu6+eVq+i6VFN9A0/YFLzeTcSPBYxvv2diIDeBtJ4JDVxfS2OrbZ1egrcaP+iYi/KVkEBwhMszm7wXJ7eoL07p+yLmg4CDkpCBhRSKRE/nwAjZBXzblSTWDZFfESBYgSywphV8ra7952+rnxD4k3EzTbrm/2FexVFsWHG0sWTRUW8PemGZCBfjcbARH0zbqWjmY9KOCRURHfgvuBm+7EBzt2F5BXBpBH7IdIsLKKjggPiTuP/O4vZ2Vybmh4CDkpHSCA3/HmUi4iW714+JLKqnfKLuQSQlDHxsu+z1hIHF6bJHMqk9ntn9mz2Xb3kviNZaYm1mfDasxDULA9nld0K5wNPWirvsqPqvg0BUJ2a9vdM37M952IjgAEr5fsln6qsCXtB3iT4JDBUaqh7KuaFBwPDUUHISclL0VjikuDNK+S1c4Vkk+x+DfzKO9gxjEpomyaafM9s/sTSw1iW8SM8ixYPuCFY6BZky1/7xKEasWl8RamdTVRC0CQW1JQAx424XgCGRsfuljY6vAj/SfL4XUFQ74qYKDKxzPDwUHISdlEBzCsEohJ/KVALn2Ho42yUt5eg+H1QvxsbI5ta/KKhbbbpO4bbexSBlCoK5UZKEQVNtkTBublPU+CynnFY7cR76HY4jd7IHZ8uoH2r63eySW938ISOixuoO66AeiovqV+A8LjlQXbf/wse4JDilHPBY77+F4Lig4CDkpOIlnwQGQ8PG3DCKhCvkJwYqc6POvVPQZJlbf67YneUtEM8Hh5S4GT7qdDZdesC+SLKi+BSTqiG0Vi21Hkq7lEgsSq/fhdd2WY9rchNn48bpI/BByfinCy6iHmyZVIBZbvmwB4eP1s98aQ26LY1cvCSGJ/2S/EqltXWR4TPj8hKgw4dCNbTMPhTrWT1LWsWJuF4LDyzkernA8DxQchJyUTnC8Fj2x56RLyAoRBr+JWLnV54/8GFBwEHJSXi04JFlgdSMEBr6ly99/XkkgZAVWRv6dVjwIOQIFByEn5ZoVDr9vA3/3gKsbhJB7Q8FByEm55SUVQgi5NxQchJwUCg5CyJmg4CDkpFBwEELOBAUHISeFgoMQciYoOAg5KYPgsF+e3P2XJnhOgvSD8wXAsxVW9dp4VjbyejCvv1GEkseFgoOQk3J3wdEIA3+SqJbtoVftL1weVXBY3+1TOyuX1K1c0/YK8ECu/O4aQh4JCg5CTsp3FxwQGOUplv5ytmjj1LZHbffmEiFwSd3KNW2vAcdIjsmr3gVDyJ2h4CDkpLSCQxIO/pbBIATM7rZhVQJJyvZn2+wx6ZlhxSNjCfdFbIgTPvwx3ksbQDy2H8Rjx62d7x/aVCY+8ptdw2+pi0e9Y15rXe8r758JivpYcn3uiewLPzYWPL57cwnEbOG7lpux5XlA3zhe07kh5I2g4CDkpCDpVMGhyRJlExG+iqDCQPZp3cYWggI2X8WwRDdbiVi+yt3aevL2JKlJc2WTtp4w1U+Kx8VNTtiz2DY+/Fu/tctCAXXDr8WiSbypi3eEeF19D8nO+HNbXO74yca8fBdJbVvKg6DIY7P2OUbfR8gjQMFByEnpBEckWStrQs7b1jaLjEGMZCzRdUndn1SaE+qAtc3xhK+VTcqDAHCS8Ih9C1ofwPqaxi1EQq91m7ZD8s9M+lExIKJjOZadfqdjM/ZerEbIW0HBQchJ6QRHiINc9mSd2g6vshcgOnAOALHfEl0VHC42QjB01La5vLJZeyTViEeS63I1ZUL1oQna+hqEgO3zuqBd4WjqRV33VXxWweErKPmtsBsm/WZfGJtfsqm+KDjIo0LBQchJOSw4qk0YLqNkkBDlXKDJzRJdbre7suHUtrm8smUfwBL075JULxUcgftIfddknuciVi0mdXfHDiZ11beMBcdj6ueSfvPYbB8FB3lUKDgIOSmHBYeUh8smJio8+aNeJFyzaQKzRBe2Wl5hdVtRccA2xCNlxKNjKCsVbSwLH0ublPU+Cxl/XuFAXV9BGO6PMHtO9sGk7S8mBJb3fwgQJt6P1vV+zG+sqpT4Ae/hII8KBQchJwWJ5qjg8DL+zsGQqE1kdDb87BX7kMBecO9BqufAhrqwa0JFW0uMK1HR2lC2JOr+kVxzvdyvJ1UIheh75UPAmDAOb+9l1MOvRvRyk9mQvHNdtM+XavLllLqy4G1xyUPnrggMvSzy4Vv92tZFhscEsbWanxi3+c1Ch5BHgYKDkJMyCA5CAMSICJ1XXXoi5M5QcBByUig4SAUrJbh0xtUN8ogMgoMQQggh5I5whYOQs8EVDkLImeAlFUJOCgUHIeRMUHAQclIoOAghZ2IpOPDzMOwPPm7r3IOP0tfnL73tEnb9iO2Xe4zP/L5YGT+Jy/Po+x+KOheJq49Fno8yN+T1tIID8yv74/jJZ++hbyCUeIefxDa2/FkE+sKzWveeWBzt8zYOgJ+4bh7EdaVPQvb4X0nun/+Uz5181n797d3Xv//u631PpoJDk2RJwCpAfJ8M4l6J47sIjpdvJ68c/zC+S9iZi1uNR7nXvM/82jxdFf+9Yv7B2QgO/CQS85z2+Wf6UUWHP9Ni9fCsKkbQ5ruLjitAvN0YcKwoOMgU+fz/+rN8RkQovOazHoJDtv/4X/nM/S6i440/b1PB0SbJnDjumES+h+CAbfPCKhvTxX2v5mJlew239ucs/OqDkOpcXcK9Yv7BqYJjeJqog7mX/cODvh4Af0T6LyKG9LPRnQgl9nb1A8JK9p8hWdfVDX+g1y8fvh0XCg4yRT7/txIcX/+QZP/+3dc/33iVYyo49r7tI2GjDYgELScC36eU1ZDP8ofnNjwVMPzldtJmEAorn0iEiFP2azJb+clYvSMJMI8z6pe+M9of7Khv/7vto4x5SLzVLnFF35Nxt/Ne9g/jKn7a+QAec2MbBEeJefA3s2Xfzfb0c5H9yfivFj5PxiA4ZF7q6kYL5lTaxTGCELF9uS2evulzDSHj9SNBSpv36W9A9yOG6tv8Vb5I3S/Yhp/Sd7Cw+dM0cz3vtz5VVP/u3C6fo/+KPeqj7P5L/GEz/zpG2/786Z+6utrSxS/gaaP5qZ913EvBUcY1vKStGbPb9NHmElOMWcTN33nMKFu/l9Sd9mn7Px2ZE9SVJDqtK4nx59THEOuvfaxIyrUu+E32428Ctv+skjb6dB/CJ0/SEuuQ8EsZ8eTPQY11ZnM/HtvQH46H/E1Fu1pHmI4rj0P6+yA+QnAIgwB5I5b3cOQk5gkvkMmJxGGgfiQE+R/tcgKOZGI2b5vb4ZtPtCu2zmdOQCs/A/AjB1X/8BfkBKf+vE3t28oxF7k82xaGsa1siFfKedzRlzCNU4CfmAP4mY278atY36u4dm3Zd7N9yeci/BM9KQ3JUo5tfiNsx+ydKlVUQkx8wX6cAG0/jkE8nht14EuSYPYdIsDj8fhmmJ9LBYfHhZMnxEc8ehz9StkTuT62PMWBuvgbyGPwmDs/mlStXhYckSRLfwO53SU2Y4in1F+NWR+pnseMBFXG7GLhkrqw5T4hDJB8vV6eE7dp3Qzqip+oawLD60YfyebJFkkcsfm3dI9Vy+IXSTzXdT9YVfpZxjn7dg8/eSUg6prPleDAvHvShx+MC8JiZdMx/kf2+xilrPXMf9hq/9bnbFzZr9rS3OW2WZR9b5aCI4OTP+qE8JDJaBNUIpKd1Y3El9vKh7MmwWiX9m1s1eclfpq6G8x/Ht+h8dTybPtIDInad9tXrWvbh5K0+dFjXJiOU5jGNbM1263/yfE8NJYfBJzQ9MSEssyLJopSZwDzK21yAneRoGIC3+hlH7b1vSRSH4mkqw9fsIXAMBtOaF4+RNPHEVsIjqYOkrHHpfVwgs7tFuWM++kEh+5HvWwr7fWYzGyrdgBtJTYXAgNN24hVtnXVIo1pVT5c1/u05AY0Uaf5GVY8SqIMzJa/1U/rCtpHIyJgm5bFZ03U2Y/7dgYBkG3VTynX/jXJy9+QC47WJnM0jc3857lZ9T+0lc9LFVV1RQP3ceDy3ikEhyIDjgRh2znB+D5PVKCtm8r1GznICfOoz10/GTk48DPEXmn6nfW9LE+2u3gHrO7RvnO9qGt1MA++f5qwq98OzFuJOb4Zr2zZ92y7lLv5Gb6Fk63gwNytTiaoI3OYRclwjGy+sc8/a0gk+XMFNJmbrfYH0eH19ISbbC0TP3u2EBU+plQni4iLBEcz3tkKRyT7Ws4gNvF/VDRkkKCmbRu/rxIRl9SVPpHQ2j59LJ7sUC6JMai2SXk4BpY0LxUc2Uf2M8RjIHH7ZYoPsxWGUq7959WRqU32VWFQ457152WPM4/rr7LaAc4jOOTDhXKXfNokAqxcE91ugkZfdqKbtTvkc+Un7cu2LnnF/uo/U22r8my7iTeweofGXcsr0Oes7hE/TZ1pXDPbbLuWJ8eTguMfBsEh6ApDMz+xH/Mr2zmBx4qFbOOXaZ+lngoP1JH6SCSXigFF/Ey/3Wde00f23dR51QqH+fF22c+rBcdrbQBjlNiOipVHWOEIG8o5UWaqLZdtOwuD+AYv21OBUcviZ0jUl9Bc4six5nLt/1YrHLP+NuVMvhRk+061wgFhUU/4w7dOGTySQyRF+QPJycyfPXEkSeZEon14u0t8ClM/qY7T2euYQ1yhnPurfVs5fM3qlnZDAs0+Doy7xr2KM+oWvwO53YIh5uJvamti2mw35e54hn+yERyYI3z7yUnTP9O+qjGIEvl/WBWxsl5OsfqakL2+HB8kFq2ft5Mt+oYvKU8TqlP9HLAhueo9AKmsJ3eUS7+HBUdph5egYS6uWuF4rc0YxmX1436KbCuxHxYRF9ZFYgyb9Fnv4dBt2FCeJcZqy2UkTfMD2x84BlK+eIVDyoNN+pgmarOFyMmCQ8o65nJfhNvQR72nxOuubHn7iMDJAmw1ri7Wru3DXlLxRBfIBGYBoicz2e8nJC8DnLQiEcrErBKLl70dEo0nycM+d/y0iB/3rch4ax34cHv4avoe5iLbZ9up7P7jxC5Mx51suX4bJyhjnM5HjW1GiXnwN7Nl37PtRVn9ybHZPZ4/GDgZ64kn75fjPSy5yrz5CUfBnEq7OEZJnAAIkprgsW9TX/wgyQx10ffCd0vnp9jcn6Mn3EU9JGW3HRYctu1zB0Gjf3OwmX9N6HkbPmq5UBN4sNNOKeO66Fcqqe6qfEndaZ8+FkuuWs6JM1NtpYz+8jH45P3LPA3JFnV3ykjA7isn3g2W9GNcqa4nbuz/H4kHfwsRK/qTvy//zMPuyXxlw5ghFDax2f48b/ADH3GZR/ZNx5X8eqx5hePhf6VCyCNBwTHSCg7yUCBhbZ4ySp6CKnCO2t4EEVUP/RwOQt4UERZY3QiBYSs1uyswPxAUHOcAlz/aFRxyas4kOB7+SaOEvDV+34bD1Y0RCo5zwFWO5+Q0ggOXWiSet17dAIPgIIQQQgi5I1zhIORscIWDEHImeEmFkJNCwUEIORMUHIScFAoOQsiZ6AWH/UJg84uAbr/tQ3uleZZFkOp2vzbwZ0ns/hJhFt+ereG7/tTywtg2pPnbnetLKHHV56+8Ot57UucicfXxzPNR5uaRaAUH4pX9MR9yLN/yd/cPS3lmSH52x4DMZ372hKMPHvueYs/iWD63g5AH5zrB0fxUUR9KNUuE1l5f7S5tBxt8yf5DJ/ccxyzWg5zm2Q6XzvWKnTm76ZxceXymzPzaPF0V/71ivjEbwSFjx0N/8k8w/TPydKJDjhES8Kt+bmptDz0V1epWWzzt9DX9E/KDcpXgQGLaCAer057w3fZZ/heyDd+qP2J/128lx1FiupSzCI6L53rFas6unM8Nt/bnLPzqEyLrXF3CvWK+MVVwDI8tdzAW2X/oqZ9nQsb1asHRAAHRzpH1sxEjK5FCCGl5veCQPzi0ueik7O3xvySF+mIu3e8+awwTG9ohDqCJt7arWNwKRI63g83aDv6snSYxE0TedqifVxpyH9mWY7NtiC+vV0VYcMFc5/mI+ugL8ePbbrKDYc7sf7dtBGC1S1zR92TMm+Nj9ds4QfGT2wx4zI1tEBwl5sHfzJZ9N9vTY5b9yfivFj47DIJD+qmrGy2IUdrFmJFkbd+wMoJHSlvs+dHmkWClzfv0mdL9iKH6Nn8DaJv7K2X0jTkOXzKXdSUBAsH7icsh5mezfwd9B42M49K3urpI0dWjpm9fVdLHg+Nvye0fxCb2qI+y+y9zGLYch21/+vRP3WG1RXz447hBjoWQt+Q6wSF/pFk07JLa42QcJ3j4QoLK/msMM9uqXgMSnZ9I6wvcsk1jSn4Qbx6vJkwvW58rP2rLsdl2JKzS3wBsB+Y6JzgdW4kvYspx1PJsWxjGtbIhXilvxmxM4xTgZ/hczMbd+FWs71Vcu7bsu9meHbPszz9b4f8ObASHzFV+9XzHsAqCNohffAziSMYJMYE3xmbhMSRm1IGv362N+Q6R4fF4fBlruxIciMuTfFy+8PZNG6+nD1tCGf1LuRMKgfnAcdq7h6Pzk98zsupb3w+S5gJ16zy6cOn8qGDIcdh2iIzSH3yEEBIb3iLaHgdCvjNvJjhw8vITt15OEX+D/xrDzLaqV2lijgTXtMvJbzghHyhn2j5sO5JrtlWauDc07Wu/075yebZ9JIbEdF5rOde17dk8DpgffG4rqzmdxjWzNdut/2Z+Do/llVwsOBCvtMlJ2kWCigmsJMg+/9v0JNfVhy9PlNmWBciU6reUXeRowhQithTHng/giTfazMDcSd1WnJjfpeBo6uSkn4XJ0G5SzoSf3Idtx8pF6X8QLYQ8EHe9pIITBeoo3QqGnaBxYo593fbKtqhX+9dykxQ0gWA8xZZFxEWCw2KIvr2PHGve7soZxDazOU2fbb+pbluebHdzN2B1j/ad60Vdq4Nj4vvzHA9Uvx2YtxJzHKeVLfuebZdyNz/1M3JrNoIDsTSJMUAdiSmLkmHMFj/2+bFDIsvHCWTBUfuD6PB606RX25ZyFRwRd+6r+mjqqJ+DiXcqTqyfTnCEGEDf0lfuO4uIiwSH9Zfne7bCETE1MeY3ig5vmSXkDekFhxCJOO2rJ+nZN7jZfvxh5BO29iH14n6ObC91p7ZVvUqJH8Q4m3Z5DmrymJbNT02gmz7ydlcuzOY09q/a7/WVy7Nt6aPOXWD1dsec6kZ5Bfqc1T3ip6kzjWtmm23XcjM/s2N2KwbBIegKQ9Nf7Ee8sp1FQqxYyDZWGuPvMSW2KiqUlQ2In71Vg2hbyt99hUO4WHDk8TV1QozI9mHBYX683eAn91H7m8UIJE7cz4FXnm9shHxnpoJDn8XQnEDzTXK+gpATDRLvXmLyE7Y/7yF8Fns+YXtfmxO/bUcMxUel8+lthwQh/0d/wmHBUdr5GJeJrCsXjsz1ENOBvto5W7Qb5if7ODDmGvcqzqhb/A7kdgtWx3Rqa2LabDfl7rMV/u9AFRzoE99qc+L0z4ivagyiRP4fVkWsrJdTrL4nf7XLeJHYtH7eTrboG76k3CZBAYnUk67H7XXRZ16tyHXDR+2v1lv1X23wJX229zlYP9UP+so3aq76Piw4SruX376VL1rhsO243CI+KTjIozAVHMATh5PFRiAf6FxHL53UOo78MQwnbGvbJj7BT9oAfeOE3p349aQqZT0x1j4qZs8+a//e5zRJ7pQ9Hu8jbDm2vN2VO8SH+1WaucZ43D6bV+AxLuOq7azs/uv4ff8w5mTL9ds4QRnjYMvU2GaUmAd/M1v2PdtelNWfHJvhs3UHkIyGb/1A5s+X0j0OT2wKYpR2MeaUsAEESV4l8H2b+uIHiW2oi74XvjMuMlAv/rbNl4qcdO7Z3DBqqDAReyRvi8nbIenWNo72n+pG8q4Un04WDF29SPjCYcFh2zEvMu5PbjP/u4IDZTkO/JUKeUSWgoMQ8nreRHA8AfWSCiHkOaDgIOQWiLDA6kYIDFup2V2BuQIKDkLImaDgIORG5EuA4J6rG4CCgxByJgbBQQghhBByR7jCQcjZeNYVDkLIc8JLKoScFAoOQsiZoOAg5KRQcBBCzgQFByEnhYLjOcFDxPAMk+6mWX8YmD7M7RbH3p/hccGDwfDY9Fl8N0fi+1X6Q3y8ifj8UHAQclIoOB4UT+KvPDYrwbGyvQZ/58rDCg7yVFBwEHJSKDgelHsJjiv9Zr78+9vn55cPAnxScJDvAAUHISdlIzhevn1bxd+y4o81l0SFpPI5PScEjxNHG319QXn8uT72uz4SHVjCq36iHvoXu9vUr8Wnz9aQuvnR7/647loXIOm6n2mCLf3Fo8xrYi5lxIIxRNsS58zmfnJ/Pr94F8vH9Hj/TR0BY/LjM4wpj0MEwAfZ3iT00re+AfZAPLPViy/Sp76DyXwsBYfUjUel1/hmMQj6yPZP3z573vZvsb8XwRJln4c/xsexh038xyUV2/4kPn2+8Pj38EEeHgoOQk4KTro5SXcvZkNiwEkbJ30XGW7zl7DBlt+Jkt+DM2DJJUQG/EjZk6f2L4lQ65pN+5cyEnm+70AFhZfNb67rD/5S8ZPaZXwlQMvor/hbCQ5NxFaGH39fy9QmZWzHO1Dy2M1/2Gr/Qn5fiq4ulLkYbFLOiTsofg/Hs8J9LgQHVjRm8WUbYvAXxaGs74WRcf7p8UrdKKNfiIc/ix/YTHyoTepVwREiw+p5fzVu8nhQcBByUnDiz4KjAgGQBUckZyvHy9IkCYTAKLYBsSE5ZT8ot3UFJMRORCzLjc/sJzMk3Iz5iIRfytpXahcrOmLf2JBgYZP+B5+Cx+Ux6zZsO/0PbWXMP0mfWVCFzcpB9rPyabZWtFTcz0xwSHw/1/gk6ed+ctuwyXaILGu7V86on4ngcJGC2MNW2pPHhIKDkJOyERxyAoZYwN+yg5O/748kXsr5Egq2h8skGWmHBJP9dOVN/2K7VHBkHwDJU/soIMl6nRAK5mOW8Kuo0NUB2YekOrXJvioMImHu9NfNC8CYuhWcQ4Ijxez2TTwzEZHZqauCq/QTokJiqGIki4g9gTGUEYf4HeZnIjhCYNQyeXgoOAg5KYPgkJMvRIQneFBXOGaCw+uijNWO6TtgZD+SUys4bDv374kT25cKjhzbIZCApZ0mY/MxEwBVVNxqhWPW36acQdzS58WCo/G5iecGggPx3X2FA36kXaxcuB8KjqejFRx+I1muiA8e6sR1YCPqysHHDWH6dsy83XDv13aD3T4sRoxJqeN9LWXsOj+pn3u+PfTV1LlIXH2c8nyUuSHXMQgOJC7MrZX9c4eTv8671M1CoQoO1NebOkVw6M2EHdIOCSb7iXJO+GLT50V4/1I+LDiqrfbp2H73X/tH8nXhoIIi2eB/dg/Fypa3h/4slkj+KTatK0RyTfY21hvewzEVEZkDdZH8Z/FlG2Ko93CozeKdlku7P+xZI1zheD5awYE/UNzQlU88ekLCSUzwfQDfiKoIwQdhlVjeXHDIBxzjzfFhHK8SHd9zrDt9vZqZX5unq+K/V8xET8ohOAT9DMtcA/xNxr0ZOAZSdyU4NGFJO63v+yrSDsmpFRxSRrLu+kcyGETEgTKSqfuaXU7xJNvVc5GhsXwS3yVOPZdZO79hdGor43VbiALbH4JDQDLFfOqvSWwfxoR9Q1uQ/HqsWawEtZ+9eCQRD+07jtRdxTeLQTgsOKTs8+V9fHKb+KfgeB76SypyIGuSQOJE2f/P+zcJqWmfuWkSnrDqA7bNidVivjiu1VhXttdwa3/Owm8krcZ2iHvFTDaC4ypwnMTfIEKeFBUVSQgctRFCrmN6D8eQlHEyksSDFQ8koEjKYo+VkJxYapJBPSkr8u1hEANW1+1DcsvtgK9AoA0SoYD92s+qj4zVO5IA4cN9Rv3Sd0b7s/G82P9u82cQDH6y/cC4czx5bG2coPhp5wN4zI1tEBwl5sHfzJZ9N9vdsyE2/mT8VwufJ+SWgiPuY2hszwYFByFvw1Rw4BKKJwCcjGbbgwgQH5vEIiAherLQNlL2hJRtq3aePLWd1csJaNXHAPxI8ppepzZyglN/3qb2XWIeyrNt4ZpxR1/CNE4BfmIO4Gc27savYn2v4tq1Zd/NdoiMhT8/nuGfKDcRHDgOtiT+I6xuAAoOQt6GqeDQBGBiQu/f8JM99lviGvbPEkuq774jETa2FdHO/K+SadRN+5QjfZp/T35g2netm8uz7SvH3fZV69r2oSRtfvAZqEzHKUzjmtma7db/5HhScIzc9JIKIYTcmbngQAKwkz5O9jWZoDzsb5IJtuu3bm+PRNPZBsyPJz/QJbhVH+HLQTJLbVuafmd9L8uT7VuNu6sXda0O5sH3TxN29dvRiIBYXVnZsu/Zdil388NLKlsoOAghZ2IuOASc5D/LSb7+VK7dP0smTTIKMdDYAvNRk2ebeFd9pH3Z1iWv2F/9Z6ptVZ5t32rctbwCfc7qHvHT1JnGNbPNtmt5cjwpOEYoOAghZ2IpOPQ+DjnxDzfzCfgGip+ODfsXySQnC/32KmVPqEMisXZqKwkSsUS74r/6qX1UOjtEVE5ywzfq3F/t28rha1a3tLtm3DXuVZxRt/gdyO0WDDEXf1NbE9Nmuyl3xzP8E4WCgxByJpaCwxNHTnB5/5AADiQTtIFIQTIJn8lWfaoIsP1oF8m1+i9+Nn102BgCSey1Dny4vcab+/Y4N7HNtlPZ/R8ad7Ll+m2coIxxOh81thkl5sHfzJZ9z7YXZfUnx2b3eP6AvKXgwM2V05tM5Tjh2QytfWUjhDw1a8FByINAwbHlTQSHiFg8iRMrnPhpOATx5hcdjyo4rO/8gK4pl9StXNOWkCeGgoM8HnLCxupGCAxbqdldgfnBeLMVDkuo+Pnoyk7B0dgI+YGh4CAPid+34XB1Y0snOPDYaZ+zZVKX+fTnb1z6YDV/TLg/4npTxxLuiwgSxIg+4tkWKxvACortB/G4cmvn+5fPypj4aB+XXupiLjCnta73lffPBEV9hDk+y+gj/NhYhsemO2YL37XcjG06D4Q8GBQchJwUJJ6csLJo0CQn5b/MVoFYGOrK3/9RwbGLiwNL3p4kNWmubNIWyXp4IRvGIDaNt4oWa1PZ+HhvL2Orydvqhl+LRZN4Uzfe/SHb+hIzi83tQdNWX2ZnY8b2e1yKOtK2lH1sEa+PzdsT8sBQcBByUgbBIYmpvgtlugLhiTztywLkaixJRt9W1thWNikPAsDxeA8m1tYHsL6yEKhEQq91m7ZD8s9M+lHBIqJjOZadfqdjI+QEUHAQclI6wYG/40wnOLrVj0suqexiSTLETy6vbNYeSdXjR3KNeFOdPaoPTdDW1yAEbJ/XBe0KR1Mv6rqv4rMKDl9ByW+Q3TDpN/vC2PySzdIXIQ8GBQchJ2VvhWPKd1rheK3gCCxB/y5J9VLBEbiP1HdN5lmUxarFpG5O/FMmddW3jAXHaernkn7z2KqNkAeEgoOQkzIIDmFYpZBEtRIg3+MejosFh22HAEgJtbuHo1u9WflY2qSs91nIPOQVDhUf5jvfw+G+2mQ/aas/IZb6y/s/BAgT70frej/mN1ZVSvyEPDoUHIScFCSbLDgAEjP+lkFOyPrUYEl4UVeSV1yCEZs+58Tqe11PlkcY2lhivFhwoGxJ1MeA5Jrr+f5I/AKEwhDvzIeAB5apuLL2XkY9/GpERZvZIBJyXbTPl2ry5ZR6I6i3xSUPzE0VGHpZ5MO3+rWtiwyPCcd0NT8xbkIeHAoOQk5KJzheC5JaFiiEEHJrKDgIOSmvFhxf5JtzFhj41ix///EtmhBC7gAFByEn5ZoVDr9vA3/3gKsbhJB7MwgOQgghhJA7ooKD//iP//iP//iP//iP//iP//iP//iP//jvzP/evfv/lz7+y1Ld620AAAAASUVORK5CYII=)

- If applicable, include Licensing Details. You do not have to put OS licenses -

- MSSQL 14.x (Server 2017) - QTY one (1)

- WebSphere 9 - QTY one (1)

- If applicable, include Database Support Level (detailed on Standard Proposal Service References in SA requirements tool).

- Database support level for this project is: Full, Partial, Minimal, No Support

- No Support requires technical exception

- If needed, indicate all application details here:

Discuss what sort of planning and tools the Agency have in place to affect installation and roll out.

Who will install the application
What sort of install instructions will be available
Will application installation be executed before or after server delivery
What sort of application installation testing will be required
Will third parties or vendors be involved in the application installation? Please detail
Are any other applications, databases or other data/systems dependencies required for application installs and/or testing.

- “Agency will be responsible and accountable for all data migration in support of the application workloads for this solution. Atos may provide migration assistance for these efforts. The agency will supply a list of the data sets, filesystems, folders, etc. required for migration.”
- “Agency will be responsible and accountable for the installation, configuration, test and validation of the application.”

**For a Refresh, you will not follow the format above\*\***:\*\*

- There is not a requirement to write a business justification, just a short summary of what is being required of Atos in the Demand.
- Indicate whether the solution will keep / change the source CI name/IP address.
- List the tasks Atos build team is responsible for (i.e. Atos will create the privileged account from the existing servers, Atos will notify the customer after cut-over has occurred)

- List the temporary server names and/or new server names.
- List the number of Temporary IP’s needed.
- Required verbiage, if applicable: “Customer is financially responsible for the source server until the associated decommissioning request is submitted.”
  “If the target servers have met the Refresh billing trigger of QA date plus 30 days, the new servers will become billable, even though they may still be in “Being Assembled” status in the CMDB.”

**Planned Payment Milestones**: ‘Please see the Cost Detail Report of this Demand for the total cost.’

Due to the upcoming contractual changes, this is to document (ERB Project Bench Worksheet') the resources, and their hours required to complete this projects scope of work. A TPC Project Bench RU will be built to mirror the resources and hours detailed in the Impact Description. It is imperative that this cost be approved by the agency for the implementation of this Demand. Failure to approve this cost will result in the stoppage of this Demand.

**Timeline**: This should be provided by the Technical Project Manager from ERB. Include the Implementation Start Date and End Date as provided by the TPM before sending the Demand for the customer for final approval.

**Example** (do not use below)

Duration: XX Business Days

Estimated Start Date: Between XX/XX/2018 and XX/XX/2018

Estimated Solution Delivery Date: Between XX/XX/2018 and XX/XX/2018

Estimated Project Completion Date: Between xx/xx/2018 and xx/xx/2018

**Deliverables**: Measurable outcomes or results or are specific items that must be produced in order to consider the project complete. Summarize the Solution Description, when filling out this section, what do we need to deliver in this Demand.

**Acceptance Criteria:** Acceptance criteria includes the process and criteria you will use to determine that the deliverables are complete and satisfactorily meet expectations.

Agency verification and server acceptance that vCPU, RAM, and storage requirements are in accordance with approved design.

Agency verification of CMDB updates.

Agency verification that correct monitoring/backup is in place per requested DR/backup levels.

Agency verification that correct ports are open and firewall rules in place to allow communication to/from servers.

Agency confirmation that installation was completed per the installation instructions provided by the agency.

**Engagement Contacts/ Resources**:

- Include your agency contact from the variables.

**Example:**

David Pustka <David.Pustka@txdmv.gov>

Texas Department of Motor Vehicles

Infrastructure Services Manager

Office: 512-465-5605 Cell: 512-422-9033

- Include the Atos resources, this should be the same as your resource plan, example:

Atos - AIX
Atos - Network
Atos - Oracle DBA
Atos - Storage
Atos - QA
Atos - Asset Mgmt
Atos - Project Mgmt
Atos - SA
Atos – BUR

**DIR Customer Responsibility**: “Agency is forecasting 20 business days for all remediation and UAT activities for this project.”

- **Please have a conversation with the customer and update the business days needed**. Otherwise the standard verbiage above is acceptable.

**Dependencies**: Enter in any dependencies. (e.g. This Demand1234 will take place once Demand5678 takes place)

**Constraints**: Anything that either restricts or dictates the actions of the Demand. Enter in any constraints. (e.g. time, scope, and cost, all of which affect quality. “this remote site only has the network capability to host 1 mbps data transfer at one time”)

**Communications** **Plan**: Enter in a communication plan if it applies. (e.g. customer can coordinate the deliver of the server only from Saturday-Sunday and should be contacted before delivery date is set via email and cell. Email and cell number is “xyz@XXX.texas.gov. XXX-XXX-XXXX)

Page Break

**Other**/**Misc**: Backup and DB BUR Information, Patch Group, User ID’s.

**Back up and Retention** **Example\*\***-\*\*

| **Server Backup and Data Retention - Patch Group Assignment** |             |               |               |              |             |      |      |
| ------------------------------------------------------------ | ----------- | ------------- | ------------- | ------------ | ----------- | ---- | ---- |
| **Server Name**                                              | **Level**   | **Frequency** | **Retention** | **Start**    | **Stop**    |      |      |
| server1                                                      | Full        | Every Friday  | 2 months      | Sat 12:01 am | Sun 4:00 am |      |      |
| server2                                                      | Full        | Every Friday  | 2 months      | Sat 12:01 am | Sun 4:00 am |      |      |
| server1                                                      | Incremental | Daily         | 2 months      | 19:00 pm     | 24:00 pm    |      |      |
| server2                                                      | Incremental | Daily         | 2 months      | 19:00 pm     | 24:00 pm    |      |      |

| **Database Backup and Data Retention**                       |                               |               |               |              |             |      |      |      |
|---|---|---|
| **Server Name**                                              | **Level**                     | **Frequency** | **Retention** | **Start**    | **Stop**    |      |      |      |
| DB server name                                               | Full                          | Daily         | 2 months      |              |             |      |      |      |
| DB server name                                               | Transaction log (Incremental) | Hourly        | 2 months      | Continuous   |             |      |      |      |
| DB server name                                               | Differential                  | Weekly        | 2 months      |              |             |      |      |      |

| **Windows OS Patch Group Assignment** |                         |
| ------------------------------------- | ----------------------- |
| **Server Name**                       | **Patching Group Name** |
| server1                               | Agency Name Prod        |
| server2                               | Agency Name DEV         |

**FIREWALLS**

Agency will be responsible and accountable for supplying firewall rules to support this deployment. Prior to Solution Design Proposal delivery or solution approval, firewall rules will be provided by Agency to open the paths for communication access from the agency site(s) to the Consolidated Data Centers with the TCP and/or UDP ports. Atos will provide assistance and consultation on the completion of the standard forms, as needed by Agency personnel.

•SA requested firewall rules from the customer. The customer did not provide firewall rules to support this solution. This will be included by default as a risk in the project by the TPM indicating a potential PCR and a delay for the solution delivery.

Or

•SA requested firewall rules to the customer. The customer provided firewall rules to support this solution. Firewall rules have been attached to the solution.

**USERS AND ADMIN ACCESS**

Agency-maintained user access.

•ULN: SA requested the accounts the customer initially wants populated during build for ULN server solutions. The customer did not provide ULN accounts to be added in this solution. This will be included by default as a risk in the project by the TPM indicating a potential PCR and a delay for the solution delivery.

Or

•ULN: SA requested the accounts the customer initially wants populated during build for ULN server solutions. The customer provided ULN accounts to be added in this solution:

oAccount 1

oAccount 2
