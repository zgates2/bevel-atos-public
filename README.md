# Readme

This repo is to hold atos process documentation and to serve as a public respository for its contents as they change over time. No customer or otherwise sensitive information should be hosted here.

[[_TOC_]]

## Links

- [Glossary](glossary.md)
- [HSC procedures](.\hsc\README.md)

## Service Now Tables:

- [Projects](https:\dirsharedservices.service-now.com\nav_to.do?uri=%2Fpm_project_list.do)
- [Demands](https:\dirsharedservices.service-now.com\nav_to.do?uri=%2Fdmn_demand_list.do)
- [HSC](https:\dirsharedservices.service-now.com\nav_to.do?uri=%2Fu_hsc_list.do)
- [Cost Plans](https:\dirsharedservices.service-now.com\nav_to.do?uri=%2Fcost_plan_list.do)
- [Expense Lines](https:\dirsharedservices.service-now.com\nav_to.do?uri=%2Ffm_expense_line_list.do)

## Maintaining this Documentation

### Replacing or Uploading a File

1. Navigate to the location where you'd like to make changes on the site. In our case we'll be updating something within the `docs` directory. ![docs-pic](media/main_docs.png)
1. Click on the `Web IDE` button which will open up a `Web editor` version of the site where we can make changes ![web ide click](media/main_web_ide.png)
1. Delete the old file that is not needed. This is done by a) Hovering over the file, b) clicking the option buttons that appear on the file name, c) selecting `Delete`. ![click delete](media/main_delete.png)
1. Upload the new file. This is done by clicking the upload icon, and following the prompts. ![upload a file](media/main_upload.png)

### Editing a document

Files with a suffix of `.md` can be edited online versus `.xlsx` files which must be deleted and reuploaded. Here's a quick process for making those updates.

1. Navigate to the location where you'd like to make changes on the site. In our case we'll be updating something within the `docs` directory. ![docs-pic](media/main_docs.png)
1. Click on the `Web IDE` button which will open up a `Web editor` version of the site where we can make changes ![web ide click](media/main_web_ide.png)
1. Click on a file that you whish to modify. ![modify click](media/main_modify.png)
1. Make the changes to the document using the editor. ![editor example](media/main_editor.png)
1. This document uses a syntax called `MarkDown` its just a text file with a few 'shortcuts' for formatting. Here's a quick table to show you some examples.

| Symbol                         | Description      | Example                                |
| ------------------------------ | ---------------- | -------------------------------------- |
| \*\*word\*\*                   | Bold             | _word_                                 |
| 1. List                        | Numbered List    | 1. List                                |
| ![link](media/main_link.png)   | Clickable link   | [example](https://bevel.work)          |
| ![image](media/main_image.png) | Display an Image | ![example](media/main_bevel.png)       |
| #                              | Title 1          | (This doesn't display well in a table) |
| ##                             | Title 2          | (This doesn't display well in a table) |

6. After completing any changes you wish to make, save your changes and publish them to the site by clicking the `Commit...` button at the bottom of the page. ![Commit](media/main_commit.png)
7. Its important to make sure the radial button is selected to `Commit to master branch` as this is _not the default option_. ![commit to master](media/main_master.png)
8. Select the the `Commit` button to finalize your changes.![push](media/main_push.png)
Sample Text
