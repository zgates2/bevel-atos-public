# HSC Guide for Technical Project Managers

[[_TOC_]]

## Summary

This document aims to introduce a `Technical Project Manager` (TPM) to their role for `HSC Dedicated - Server/Appliance` hardware service charges.

## Introduction

**Hardware Service Charges** are costs assessed to a customer for cables, servers, blades or appliances purchased specifically on behalf of a customer that is dedicated to a single customer's use.
Examples of this would be:

1. A dedicated server to host 'xyz' in the `Consolidated Datacenter` (CDC).
2. A vendor appliance that is purchased on behalf of a customer.
3. Equipment not regularly used by the program but procured on behalf of a specific customer.

This specifically excludes things like:

1. Vblock consumption
2. Cloud Usage
3. Shared Network Usage

_Note that `HSC Dedicated - Servers/Appliances` can be purchased through the program for assets that will ultimately reside at a customer location._

## TPM's Role

The TPM will be responsible for:

1. Renewal of any expired `HSC Dedicated - Servers/Appliance` quotes.
2. Opening of a `Project Change Request` (PCR) if there is price change. In this scenario the `Solution Architect` (SA) will engage to update or rebuild the `HSC Dedicated - Server and Installation CET` to confirm that it is accurate.
3. [Creating the 'Service Now Expense Lines' after the Build Validation Letter or Project Acceptance Letter (whichever is first) has been approved by the customer.](creating_expense_lines.md)
4. [Confirming all 'Service Now Expense Lines' are moved to 'Processed'.](update_expense_line_to_processed.md)

## TPM FAQ

### Q: Why must the 'HSC Dedicated - Server and Installation CET' be used if there's a small cost change?

A: The `HSC Dedicated - Server and Installation CET` provides the appropriate sales tax, `Personal Property Taxes` (PPT), and installation costs associated with the asset. This spreadsheet is maintained by the finance team to contain the appropriate rates. It also takes into account where the device will be located as for example the tax rates differ between the ADC and SDC. The CET also assesses a installation charge per the program design where appropriate (e.g. if the installation is not for MISC component)

### Q: What support is needed by the TPMs should a new quote be generated?

A: Generally it will not be required, as the cost quoted in the initial and the regenerated quote are the same. There may be cases however, where the total dollar amounts have changed significantly and the `HSC Dedicated - Server and Installation CET` forms will need to be updated and presented to the customer. The SA is best equipped to have these conversations.

### Q: When will the customer be invoiced for HSC Dedicated - Server/Appliance charges?

A: Agencies will provide a final approval for procurement in the `Project Kickoff Meeting` when they are presented a timeline for the project and information about the finalized quotes (if needed). At this point, the customer will be responsible for these charges, however the trigger for them to be placed on a customer invoice is the `Build Validation Letter` where the customer will have a chance to QA the equipment.

All costs, including maintenance are processed as a one-time charge. Specifically when the expense line is moved to `Processed` it will be included in the next billing cycle.

### Q: What happens if the customer no longer needs an 'HSC Dedicated - Server/ Appliance' they've ordered?

A: If this is prior to use, Atos will attempt to **Return/Replace/Rehome** the equipment on a best-effort basis. However, in the event that this is not possible the customer will still be responsible for the costs associated with the equipment.

### Q: What maintenance should be included?

A: All `HSC Dedicated - Servers/Appliances` are treated as one-time costs and should include `60 months` or `5 years` of maintenance. If the vendor does not support these terms, please inform the procurement department as all vendors should provide options for this.

### Q: How many expense lines are needed, and how are they handled for PCRs?

![hla image](../media/tpm_hla.png)

A: Above is a quick graphic that walks through the steps, but this can be broken down into a few parts:

1. For each server or CI there will need to be an expense for any installation costs, and for the hardware itself.
2. If there is a PCR prior to the expense lines being moved to `Processed` you can simply update the existing `Expense Lines`.
3. If not, new `Expense Lines` will need to be created for the PCR costs.

_Note:_ In either case (pre or post `Processed`) the totals used for the `Expense Line` should be documented within a _versioned_ CET provided by the _Architect_.

## More information

1. [Link to a general FAQ](faq.md)
