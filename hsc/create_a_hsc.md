# Creating an HSC Service Now Recrod

## TOC

[[_TOC_]

## Procedure

1.Navigate to the Service Now Demand itself

1.  Load `related lists` \
    ![image-20201013135623516](../media/image-20201013135623516.png) \
    if necessary and click on `Hardware Service Charges` and then `new` \
    ![image-20201013140505470](../media/image-20201013140505470.png)
1.  Fill out the HSC Service Now Record with the required information, required information includes:

| Name                                  | Notes                                                                                                                                     | Example            |
| ------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------- | ------------------ |
| Demand                                | Enter the Demand id number                                                                                                                | DMND1234567        |
| New Server                            | Checked for non refreshes                                                                                                                 | `Check`            |
| Proposed Configuration Item (CI) Name | If a CI name is applicable, name this per the agency naming convention                                                                    | dmvdxvsql03-t      |
| Hardware Type                         | HSC's will always be physical appliance or physical server                                                                                | Physical Appliance |
| Start date                            | Estimated order date                                                                                                                      | 2020-09-01         |
| End Date                              | Useful life of device (Generally 5 years)                                                                                                 | 2025-09-01         |
| Quote                                 | Pretax total amount in dollars                                                                                                            | \$100.00           |
| TotalCost+Tax+PPT                     | Post-tax total, this number can be created by entering the total into the HSC Server and Installation CET if not included with the quote. | \$110.00           |
| Installation Fee                      | This is provided from the HSC Server and Installation CET                                                                                 | \$100              |

Here's a quick screenshot of what you can expect to be looking at for each of these fields:

![image-20201013140555388](../media/image-20201013140555388.png)
