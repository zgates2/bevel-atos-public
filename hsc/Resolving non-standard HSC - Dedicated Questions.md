# Resolving non-standard HSC - Dedicated Questions

## Pricing Questions

If there are questions or anomalies within the `CET`, `HSC Service Now` Records, the `Cost plan`, or `Vendor Quotes`. These questions should be referred to the SA that worked the Demand.

Please mark the email as **Urgent** Cost Question and generally response will be within a day or two.

## Project Questions

If an expense line was not created or with a number that cannot be confirmed this would be a question for the `TPM` that worked the project.

If the amount turns out to be correct, corroborating information must be appended to the project record (Email, quote, etc).



PRJ0407505 -- Physical appliance quote, installation (y/n). John Davila (Atos)

PRJ0290880 -- J Polacheck for details amount entered in the HSC looks to be pretax, also the hardware was retired shortly after deployment, what gives?