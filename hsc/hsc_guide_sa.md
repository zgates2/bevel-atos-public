# HSC Guide for Solution Architects

[[_TOC_]]

## Summary

This document aims to introduce a `Solution Architect` (SA) to their role for `HSC Dedicated - Server` and `HSC Dedicated - Appliance` Hardware Service Charges. This will include:

1. Getting a quote from the upstream vendor
1. Applying the appropriate taxes to the quote.
1. Understanding which `Resource Units` (RUs) are appropriate.

## Introduction

**Hardware Service Charges** are costs assessed to an agency for cables, servers, blades or appliances purchased specifically on behalf of an agency that is **dedicated to a single agency's use**.

Examples of this would be:

1. A dedicated server to host 'xyz' in the CDC.
2. A vendor appliance that is purchased on behalf of an agency.
3. Equipment not regularly used by the program but procured on behalf of a specific agency.

This specifically excludes things like:

1. Vblock consumption
2. Cloud Usage
3. Shared Network Usage

_Note that HSCs can be purchased through the program for assets that will ultimately reside at an agency location._

## Solution Architect's Role

The `SA` will be responsible for helping the agency find the desired equipment source it with a supported vendor and size it appropriately based on the agency's requirements. This may take the form of a bill of materials that the agency asks to purchase on their behalf or through a requirements and research approach.
The SA will provide the following for each demand that contains HSCs:

1. Original quotes from the vendor attached to the Demand.
1. [A CET that will assess the appropriate Taxes, and installation fees to the quote.](proposed_simplified_hsc_cet.md)
1. [HSC entries for each purchase within Service Now.](create_a_hsc.md)
1. Support as needed for the Project Team should the quote expire prior to being processed.
1. Support as needed for the Project Team should a `Project Change Request` (PCR) be needed.

## Solution Architect FAQ

### Q: Why must the 'HSC Dedicated - Server and Installation CET' be used?

A: The `HSC Dedicated - Server and Installation CET` provides the appropriate sales tax, 'Personal Property Taxes' (PPT), and installation costs associated with the asset. This spreadsheet is maintained by the finance team to contain the appropriate rates (both taxes as they're updated, and the RUs themselves which change on fiscal year, or ammendment basis). It also takes into account where the device will be located as for example the tax rates differ between the ADC and SDC. The CET also assesses a installation charge per the program design where appropriate (i.e. if the installation is not for a MISC item)

### Q: What support is needed by the TPMs should a new quote be generated?

A: Generally it will not be required, as the cost quoted in the initial and the regenerated quote are the same. There may be cases however, where the total dollar amounts have changed and the CET forms will need to be updated and presented to the agency. This will be done through a `Project Change Request` (PCR). The Solution architect is best equipped for this effort.

### Q: How much are the installation fees?

A: The rates are around $1,170.93 for each server and $429 for each appliance. This amount increases slightly each year after FY2021. In the event a 3rd party is used to perform the installation, the costs associated with it are passed directly to the customer this can occur at times during high demand or for some remote installations.

**Note:** As of 9/1/2020, there are two RUs for `Dedicated HSC installations`

1. The `Dedicated Server Installation Fee` which is assessed for **non-refresh** efforts.
2. The `Physical Appliance Installation` which is assess for **all appliance installations when a 3rd party is not used**.

More information on this and the contract sections associated can be found in the [glossary](../glossary.md).

### Q: When will the agency be invoiced for Dedicated HSC - Server/Appliance costs?

A: Agencies will provide a final approval for procurement in the `Project Kickoff Meeting` when they are presented a timeline for the project and information about the finalized quotes (if needed). At this point, the agency will be responsible for these charges, however the trigger for them to be placed on an agency invoice is the `Build Validation Letter` (BVL) where the agency will have a chance to QA the equipment.

All costs, including maintenance are processed as a one-time charge (OTC).

### Q: What happens if the agency no longer needs an HSC they've ordered?

A: If this is prior to use, Atos will attempt to **Return/Replace/Rehome** the equipment on a best-effort basis. However in the event that this is not possible, the agency will still be responsible for the costs associated with the equipment.

### Q: How much maintenance should be included?

A: All `HSC Dedicated - Server/Appliance` purchases are treated as one-time-costs and should include `60 months` or `5 years` of maintenance. If the vendor does not support these terms, please inform the procurement department as all vendors should provide options for this.

### Q: What pricing is used for HSCs?

A: All HSCs are provided at cost to the agency. The original vendor quote (pre-tax) is included for agency review for each HSC purchase.

### Q: What if the Vendor Quote includes a post tax number (e.g. Quote + Sales Tax)?

A: Enter the pretax total into the CET. This will assure the correct/consistent tax is applied.

## More information

1. [Link to a general FAQ](faq.md)

## Outstanding questions

1. How are MISC `HSC Dedicated - Server` costs being entered today? What hardware type is being entered for them (we only see two options sever/appliance)?
