How to tell if a given record needs `HSC Dedicated` charges

## Configuration Item

Unfortunately there is not a guaranteed method to determine if a `Configuration Item (CI)` is or is not an `HSC - Dedicated` server or appliance. However, in some cases it can be established.

1. If the location is not in the ADC this is an indication of dedicated equipment: ![](../media/ident13.png)
2. If under the `Chargeback` tab the `Remote Server Break Fix` is indicated this most likely points to dedicated equipment: ![](../media/ident14.png)
3. If the listed `company` is for a generic customer (e.g. DCS `Shared Company`, or `Atos`) it is **not** `HSC - Dedicated` ![](../media/ident15.png)

## Demand	

### Hardware Service Charge Record

The solution architect will create `Hardware Service Charge` records for each billable component of the HSC. This will include separate items for the procurement of Hardware and installation costs. Here's a quick process for identifying them:

1. Navigate to the `Demand` in `Service Now`. 

2. Go to the `Related Items` table at the bottom of the page.

3. Click on `Hardware Service Charges` tab. 

   ![](../media/ident10.png)

4. HSC - Dediated items will be listed as `Hardware Type: Dedicated`. Note that not all `HSC - Dedicated` costs will have a `Configuration Item (CI)` related to them, as for things like cables or incidentals they may not be applicable.

5. Within the `HSC` you can see quite a bit of information about the asset including what type of charge is being listed.

6. The total listed within the `HSC` records should match the **Post tax amounts** found in an attached CET.

7. The `Hardware Type` within the `HSC` record should be `Phsyical Server` not `Virtual Server/Appliance`.

   ![](../media/ident17.png)

### Cost Plans

`Cost Plans` are used to provide the quoted expenses for the given project. You should be able to find the amounts related to a given server within the cost plan.

![](../media/ident11.png)



**TODO Formatting**

It can be helpful to add the `Resource Unit` label to the costplan table. With this you can identify the type of Costplan item that is being worked. 

![](../media/ident16.png)

For each item in the Cost plan there should be an attached quote for each hardware item. Note that software for dedicated equipment or installation fees may not have an attached quote.





### CET Attached

_Note: If there has been `PCRs` related to this project please review the PCRs for the latest version of the CET to be used_

1. If there is a CET attached with CET costs. This is seen on the top of the `Project` or `Demand`: 

![](../media/ident2.png)

![](../media/ident1.png)

2. This CET will contain the details if they apply to this specific demand.
   1. In the legacy version CET this will look like this: ![](../media/ident3.png)
   2. The `HSC - Dedicated Server and Appliance CET` will look like this:![](../media/ident4.png)

## Project

### CET Attached

_Note: If there has been `PCRs` related to this project please review the PCRs for the latest version of the CET to be used_

1. If there is a CET attached with CET costs. This is seen on the top of the `Project` or `Demand`: 

![](../media/ident2.png)

![](../media/ident1.png)

2. This CET will contain the details if they apply to this specific demand.
   1. In the legacy version CET this will look like this: ![](../media/ident3.png)
   2. The `HSC - Dedicated Server and Appliance CET` will look like this:![](../media/ident4.png)

### Project Schedule

If there is no attached CET a hardware purchased can be ruled out by looking at the schedule and seeing if there was a procurement step within the project. This can be helpful to see if the hardware referenced within the project was provided by the agency or purchased as part of the project.

This can be done by:

1. Navigate to the `Project` within `Service Now`
2. Click on the `Hamburger Bars` on the top left near the `Project` ID.![](../media/ident5.png)
3. Click on the `Planning Console` from the drop-down menu. ![](../media/ident6.png)
4. Expand the `Project Tree` below until presented with the various phases of the project. Hardware procurement should take place shortly after the kickoff meeting and early in the `Execution Phase` as seen here: ![](../media/ident7.png)
5. Oftentimes there is supplemental notes within the `Status Report` on this same screen. ![](../media/ident8.png)

6. Here's an example within the `Status Report`: ![](../media/ident9.png)

   