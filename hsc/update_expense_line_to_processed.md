# Processing (finalizing) an Expense Line

## Summary

If the agency has validated the `Build Validation Letter` or `Project Acceptance Letter` (Sometimes these are sent at the same time.), we are able to finalize and invoice the agency for the HSCs procured on their behalf.

## TOC

[[_TOC_]]

## Requirements

Note prior to running the below process you'll need to have:

1. An approved quote from the vendor.
2. Ordered and received the equipment.
3. Equipment has gone through the staging process and created a configuration item.
4. That configuration item has been moved to `being assembled` by the build team.
5. The agency has approved the `build validation letter`.

## Process

1. Login to Service Now:

   1. Navigate to [https:\\ssologin.sharedservices.dir.texas.gov]()
   2. Enter email address and password
      ![sso](../media/sso.png)

1. Switch to the Service Now portal view.
   ![sn](../media/service-now-portal.png)

1. Navigate to the appropriate HSC

   1. Option **one** is simply enter the full HSC number into the search bar of the service now portal:
      ![search](../media/search-sn.png)
   1. Option **two** is to navigate to the [Expense Line Table](https:\dirsharedservices.service-now.com\nav_to.do?uri=%2Ffm_expense_line_list.do) and find the expense line you're after.
   1. Option **three** go to the source project and select the appropriate expense line found in the **Related Lists** section.

1. Click on the `state` field and change to the value `processed`. Save the changes to the record
   ![el](../media/processed_el.png)
