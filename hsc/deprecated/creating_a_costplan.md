# Create a new cost plan

## TOC

[[_TOC_]]

## Process

1. Navigate to the demand:
   ![image-20201013135335150](../media/image-20201013135335150.png)
   1. Load `Related Lists`
      ![image-20201013135623516](../media/image-20201013135623516.png)
   2. click on `Cost Plans` then click the `New` button to create a new entry.
      ![image-20201013135438292](../media/image-20201013135438292.png)

## Components of a HSC

_TODO_

<Create the new cost plan items>

## Attach quote where number is generated to the demand itself

_TODO_

## Attach a cet for the Demand
