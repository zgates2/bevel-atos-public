# CET Walkthrough for calculating HSC Costs

For the most updated version of this document please navigate [here](https://gitlab.com/zgates2/bevel-atos-public/-/blob/master/hsc/README.md).

## TOC

[[_TOC_]]

## FAQ for Calculating HSC Charges

1. Q: How much do installations cost?

   A: Installation costs are fixed by the program. Appliances are charged \$429/Device, and Servers \$1,172/Server. These costs are waived for **Refreshes**.

2. Q: How is the tax rate assessed?

   A: By selecting the site associated with the line item the appropriate tax rate will be added to the total. Tax rate is determined by the device final location at the close of the project and rates are updated periodically within the CET by the BOS and Finance Teams.

So the Cost Estimation Table or CET serves as the authorized means of providing Hardware Service Charges (HSC) totals that include the appropriate tax and installation costs. The decision criteria follow this flow:

## Adding HSC Charges for Named Configuration Items

**Note**: this should exclude costs for cables and misc components. You should also not add manually any installation charges to the quotes. They will be automatically added when we adjust the value with column **"AC"**.

1. List each HSC that will be tied to a Configuration Item (CI). This would be things like the dedicated servers and hardware themselves. This is done in the **Dedicated Hardware Service Charge** table in column **"A"**. Show in the image below as <u>underlined</u>.

   ![cet-1](../media/cet-1.png)

2. Enter the CI Name per the appropriate naming schema into column **"B"**.

   ![](../media/cet-8.png)

3. Enter the date that the proposal from the vendor was generated in column **"C"**. This will be used by the TPM team to judge if the quote is still current for each of the assets or if it will need to be regenerated.

   ![](../media/cet-9.png)

4. Enter the Year, for each CI in column **"H"**. All HSCs are **One time Purchases**, this should be the year it is anticipated the purchase to be done in. **Note** that this is the fiscal year for example FY 2021 from September 1st 2021 or 09/01/2021.

![image-20201104114739018](../media/cet-11.png)

3. Indicate whether this CI is a **Refresh** of an existing asset or a net **New** device in column **"J"**. This will affect if an installation charge is assessed for this CI.

   ![](../media/cet-2.png)

4. Select the associated site where the asset will reside at the end of the project from the dropdown list in Column **"L"**. This will affect what Personal Property Tax (PPT) is assessed to the purchase price. The tax rate will automatically be filled depending on the site selection.

   ![](../media/cet-3.png)

   **Note:** it is very important to download a new copy of the CET each time when generating a quote, as the tax rates within the CET will be updated periodically by the finance team.

5. Enter the **pre-tax** quoted amounts into column **"Z"**. This should be for the **grand total** of all hardware and maintenance for 5 year period. If the vendor is unable to provide **5 years** or **60 months** of maintenance please escalate to the <u>BOS</u> and <u>Procurement</u> teams.

![](../media/cet-4.png)

6. Enter "OTC" for each listed CI within the Payment Type Field in column **"AC"**. This will show this asset as a one-time-charge.

   ![](../media/cet-5.png)

7. Within the fiscal year section indicated within the **"H"** column set the **"Months"** column to one which will start the automatic calculations of the CET. For example for Fiscal Year 2020 we have updated column **"AG"** to "1" for each of the CIs.

   ![](../media/cet-6.png)

8. Confirm that the totals have shown up automatically in column **"AZ"**.

   ![](../media/cet-10.png)

## Adding HSC Charges for Misc Components

**Note**: This should apply to things like cables, rackmounts, screws, etc.

This process is nearly identical to that of the Dedicated Server procedure. The only differences are:

1. Use of the Miscellaneous Hardware Service Charge Table\*\* that is below the dedicated table.
2. Within the **CI Name** field in column **"B"** indicate the CI the costs are most associated with **or the project ID** the resources support.
3. There is no installation charges associated with misc components.
4. The New/Refresh column (**"I"**) will always be set to **New**

Here is the process laid out:

1. List each HSC that will be tied to a Configuration Item (CI). This would be things like the dedicated servers and hardware themselves. This is done in the **Dedicated Hardware Service Charge** table in column **"A"**. Show in the image below as <u>underlined</u>.

![cet-1](../media/cet-1.png)

2. Enter the CI Name per the appropriate naming schema into column **"B"**.

   ![](../media/cet-8.png)

3. Enter the date that the proposal from the vendor was generated in column **"C"**. This will be used by the TPM team to judge if the quote is still current for each of the assets or if it will need to be regenerated.

   ![](../media/cet-9.png)

4. Enter the Year, for each CI in column **"H"**. All HSCs are **One time Purchases**, this should be the year it is anticipated the purchase to be done in. **Note** that this is the fiscal year for example FY 2021 from September 1st 2021 or 09/01/2021.

![image-20201104114739018](../media/cet-11.png)

3. Indicate whether this CI is a **Refresh** of an existing asset or a net **New** device in column **"J"**. This will affect if an installation charge is assessed for this CI.

   ![](../media/cet-2.png)

4. Select the associated site where the asset will reside at the end of the project from the dropdown list in Column **"L"**. This will affect what Personal Property Tax (PPT) is assessed to the purchase price. The tax rate will automatically be filled depending on the site selection.

   ![](../media/cet-3.png)

   **Note:** it is very important to download a new copy of the CET each time when generating a quote, as the tax rates within the CET will be updated periodically by the finance team.

5. Enter the **pre-tax** quoted amounts into column **"Z"**. This should be for the **grand total** of all hardware and maintenance for 5 year period. If the vendor is unable to provide **5 years** or **60 months** of maintenance please escalate to the <u>BOS</u> and <u>Procurement</u> teams.

![](../media/cet-4.png)

6. Enter "OTC" for each listed CI within the Payment Type Field in column **"AC"**. This will show this asset as a one-time-charge.

   ![](../media/cet-5.png)

7. Within the fiscal year section indicated within the **"H"** column set the **"Months"** column to one which will start the automatic calculations of the CET. For example for Fiscal Year 2020 we have updated column **"AG"** to "1" for each of the CIs.

   ![](../media/cet-6.png)

8. Confirm that the totals have shown up automatically in column **"AZ"**.

   ![](../media/cet-10.png)
