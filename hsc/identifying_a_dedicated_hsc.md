# How to identify something as an HSC

[[_TOC_]]

## Configuration Item

[SSO Link](https://dirsharedservices.service-now.com/nav_to.do?uri=%2Fcmdb_ci_server_list.do%3Fsysparm_query%3Du_functional_category%3DSR_400%26sysparm_first_row%3D1%26sysparm_view%3D)

Easiest way to tell is that the `Functional Category` = `Server Device`
